# ciqual-csv-json-mapper

The script `ciqual-csv-json-mapper.py` allows to convert a Ciqual csv into json.  
It requires Python (2 or 3).  
It deduces the mappings from csv to json given the samples located at `resources/` (a sample source in csv, a sample target in json).  

TODO: this script will be called from the web, so it will be later adapted to CGI.

## Getting started

### 1) Download the sources

Either pull the sources via Git :
  
```bash
git clone git@bitbucket.org:samplme/ciqual-csv-json-mapper.git
cd ciqual-csv-json-mapper
```

Or download the sources via the zip :

```bash
curl https://bitbucket.org/samplme/ciqual-csv-json-mapper/get/HEAD.zip
unzip HEAD.zip
cd samplme-ciqual-csv-json-mapper-*/
```

### 2) Execute the script

Finally, test the script (it will show you the mapping details/issues before showing you the json) :

```bash
python ciqual-csv-json-mapper.py < resources/food-with-10350-10200-and-id-1234-for-testing.csv
```

## Usage

To generate `your.json` given `your.csv` :

```bash
python -O ciqual-csv-json-mapper.py < your.csv > your.json
```

To show the json given `your.csv` :

```bash
python -O ciqual-csv-json-mapper.py < your.csv
```

To show the json in debug mode (without -O)  given `your.csv` (it will show you the mapping details/issues) :

```bash
python ciqual-csv-json-mapper.py < your.csv
```
