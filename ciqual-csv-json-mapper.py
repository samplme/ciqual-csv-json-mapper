#!/usr/bin/env python

import csv
import json
import sys

# mapping configuration
sample_csv_path = 'resources/food.csv'
sample_json_path = 'resources/food_export.json'
sample_json_field_paths_to_replace = [ "/nutrients/amount", "/id", "/name"] #, "/comments"]

# csv (captured from the standard input) to be converted into json
final_csv_file = sys.stdin

def normalize_sample_value(sample_value):
    # we want to convert "11646775" (sample value from csv) into "11646775.00" to be able to match with the normalized json value
    # and we want to convert 1008 (sample value from json) into "1008.00" (sample value from csv) to be able to match them
    if (type(sample_value) == str and sample_value.isdigit()) or isinstance(sample_value, (int, float)):
        return "%.2f" % float(sample_value)
    else:
        return str(sample_value)

def cast_csv_final_value_like_json_sample_value(csv_final_value, json_sample_value):
    if isinstance(json_sample_value, float):
        return float(csv_final_value)
    elif isinstance(json_sample_value, int):
        return int(float(csv_final_value)) # since we may have "1008.00" from csv but 1008 from json
    else:
        return csv_final_value

def find_immediate_parents(json_data, json_field_path):
    if json_field_path.startswith("/"):
        parent_fieldnames = json_field_path[1:].split('/')[:-1] # exclude the last field to get immediate parents
    else:
        raise Exception("Only absolute json field paths are supported (invalid %s)" % json_field_path)
    current_parents = [json_data] # root node
    while len(parent_fieldnames) > 0:
        next_parent_fieldname = parent_fieldnames.pop(0)
        next_parents = []
        for current_parent in current_parents:
            next_parent_node = current_parent[next_parent_fieldname]
            if isinstance(next_parent_node, list):
                next_parents.extend(next_parent_node)
            else:
                next_parents.append(next_parent_node)
        current_parents = next_parents
    return current_parents

def extract_last_field_name(json_field_path):
    return json_field_path.split('/')[-1]

def debug(message):
    if __debug__:
        print(message)

def nth(index):
    if index == 1:
        return "1st"
    elif index == 2:
        return "2nd"
    elif index == 3:
        return "3rd"
    else:
        return "%dth" % index

# load the csv-header -> normalized-sample-value mapping from the sample csv
csvheader_normalizedsamplevalue_mapping = {}
with open(sample_csv_path, 'r') as sample_csv_file:
    csv_reader = csv.reader(sample_csv_file, delimiter=';')
    csv_headers = next(csv_reader)
    csv_first_row = [row for row in csv_reader][0]
    for i in range(len(csv_headers)):
        csv_header = csv_headers[i]
        csv_samplevalue = csv_first_row[i]
        csvheader_normalizedsamplevalue_mapping[csv_header] = normalize_sample_value(csv_samplevalue)

#debug("csvheader_normalizedsamplevalue_mapping = %s" % csvheader_normalizedsamplevalue_mapping)

# load the csv-header -> final-value mapping from the final csv
csvheader_csvfinalvalue_mapping = {}
with final_csv_file:
    csv_reader = csv.reader(final_csv_file, delimiter=';')
    csv_headers = next(csv_reader)
    csv_first_row = [row for row in csv_reader][0]
    for i in range(len(csv_headers)):
        csv_header = csv_headers[i]
        csv_finalvalue = csv_first_row[i]
        csvheader_csvfinalvalue_mapping[csv_header] = csv_finalvalue

#debug("csvheader_csvfinalvalue_mapping = %s" % csvheader_csvfinalvalue_mapping)

# create the normalized-sample-value -> csv-header mapping to be able to bind the above mappings
normalizedsamplevalue_csvheader_mapping = {v: k for k, v in csvheader_normalizedsamplevalue_mapping.items()}

#debug("csv_normalizedsamplevalue_fieldname_mapping = %s" % csv_normalizedsamplevalue_fieldname_mapping)

# generate the final json (by replacing any normalized-sample-value with the final-value given configured field paths)
with open(sample_json_path, 'r') as sample_json_file:
    json_data = json.load(sample_json_file)
    for json_field_path in sample_json_field_paths_to_replace:
        fieldname = extract_last_field_name(json_field_path)
        i = 1
        for immediate_json_parent in find_immediate_parents(json_data, json_field_path):
            json_samplevalue = immediate_json_parent[fieldname]
            csv_header = normalizedsamplevalue_csvheader_mapping.get(normalize_sample_value(json_samplevalue))
            if csv_header is None:
                debug("WARN: %s %s sample value %s not found in the sample csv..." % (json_field_path, nth(i), json_samplevalue))
            else :
                csv_finalvalue = csvheader_csvfinalvalue_mapping.get(csv_header)
                if csv_finalvalue is None:
                    debug("WARN: header '%s' not found in the csv..." % csv_header)
                else :
                    json_finalvalue = cast_csv_final_value_like_json_sample_value(csv_finalvalue, json_samplevalue)
                    debug("INFO: %s %s sample value %s has been properly replaced with %s..." % (json_field_path, nth(i), json_samplevalue, json_finalvalue))
                    immediate_json_parent[fieldname] = json_finalvalue
            i += 1

# print the final json
print(json.dumps(json_data, indent=2))